goto new

#replace ; with tab
awk  '{gsub(";","\t",$0); print;}' <../input/test.csv >test.txt

# cut FD003
#awk '{if (substr($0,1,5)=="FD003") print $0}' <test.csv >FD003.txt

# cut FD003
awk '{if ($1=="FD003") print $0}' <test.txt >FD003.txt

# cut FD003, unit 1
awk '{if ($2=="1") print $0}' <FD003.txt >FD003_u1.txt

#get min, max
gmt info FD003_u1.txt > FD003_u1.info

gmtset PS_MEDIA 300px600p
gmtset FONT_ANNOT_PRIMARY 8
gmtset FONT_LABEL 8
gmtset MAP_LABEL_OFFSET 4p

#plot sensor 2
gmt psbasemap -R0/250/640/645 -JX10c/6c -V -K -BWeSn -Bxa50g50+l"cycle" -Bya1g0.5+l"units">FD003_u1_s2.ps
gmt psxy FD003_u1.txt -R0/250/640/645 -JX10c/6c -i2,7 -W1,blue -V -O >>FD003_u1_s2.ps
gmt ps2raster -Tg -V FD003_u1_s2.ps -E300 -A -P

#plot sensor 3
gmt psbasemap -R0/250/1570/1600 -JX10c/6c -V -K -BWeSn -Bxa50g50+l"cycle" -Bya10g5+l"units">FD003_u1_s3.ps
gmt psxy FD003_u1.txt -R0/250/1570/1600 -JX10c/6c -i2,8 -W1,blue -V -O >>FD003_u1_s3.ps
gmt ps2raster -Tg -V FD003_u1_s3.ps -E300 -A -P

#plot sensor 21
gmt psbasemap -R0/250/23.2/23.7 -JX10c/6c -V -K -BWeSn -Bxa50g50+l"cycle" -Bya0.1g0.05+l"units">FD003_u1_s21.ps
gmt psxy FD003_u1.txt -R0/250/23.2/23.7 -JX10c/6c -i2,26 -W1,blue -V -O >>FD003_u1_s21.ps
gmt ps2raster -Tg -V FD003_u1_s21.ps -E300 -A -P

# cut FD003, unit 4
awk '{if ($2=="4") print $0}' <FD003.txt >FD003_u4.txt

#get min, max
gmt info FD003_u4.txt > FD003_u4.info

new:

gmtset PS_MEDIA 300px600p
gmtset FONT_ANNOT_PRIMARY 8
gmtset FONT_LABEL 8
gmtset MAP_LABEL_OFFSET 4p

#plot sensor 2
gmt psbasemap -R0/250/640/645 -JX10c/6c -V -K -BWeSn -Bxa50g50+l"cycle" -Bya1g0.5+l"units">FD003_u4_s2.ps
gmt psxy FD003_u4.txt -R0/250/640/645 -JX10c/6c -i2,7 -W1,blue -V -O >>FD003_u4_s2.ps
gmt ps2raster -Tg -V FD003_u4_s2.ps -E300 -A -P

#plot sensor 3
gmt psbasemap -R0/250/1570/1600 -JX10c/6c -V -K -BWeSn -Bxa50g50+l"cycle" -Bya10g5+l"units">FD003_u4_s3.ps
gmt psxy FD003_u4.txt -R0/250/1570/1600 -JX10c/6c -i2,8 -W1,blue -V -O >>FD003_u4_s3.ps
gmt ps2raster -Tg -V FD003_u4_s3.ps -E300 -A -P

#plot sensor 21
gmt psbasemap -R0/250/23.2/23.7 -JX10c/6c -V -K -BWeSn -Bxa50g50+l"cycle" -Bya0.1g0.05+l"units">FD003_u4_s21.ps
gmt psxy FD003_u4.txt -R0/250/23.2/23.7 -JX10c/6c -i2,26 -W1,blue -V -O >>FD003_u4_s21.ps
gmt ps2raster -Tg -V FD003_u4_s21.ps -E300 -A -P

# cut FD003, unit 6
awk '{if ($2=="6") print $0}' <FD003.txt >FD003_u6.txt

#get min, max
gmt info FD003_u6.txt > FD003_u6.info


gmtset PS_MEDIA 300px600p
gmtset FONT_ANNOT_PRIMARY 8
gmtset FONT_LABEL 8
gmtset MAP_LABEL_OFFSET 4p

#plot sensor 2
gmt psbasemap -R0/250/640/645 -JX10c/6c -V -K -BWeSn -Bxa50g50+l"cycle" -Bya1g0.5+l"units">FD003_u6_s2.ps
gmt psxy FD003_u6.txt -R0/250/640/645 -JX10c/6c -i2,7 -W1,blue -V -O >>FD003_u6_s2.ps
gmt ps2raster -Tg -V FD003_u6_s2.ps -E300 -A -P

#plot sensor 3
gmt psbasemap -R0/250/1570/1600 -JX10c/6c -V -K -BWeSn -Bxa50g50+l"cycle" -Bya10g5+l"units">FD003_u6_s3.ps
gmt psxy FD003_u6.txt -R0/250/1570/1600 -JX10c/6c -i2,8 -W1,blue -V -O >>FD003_u6_s3.ps
gmt ps2raster -Tg -V FD003_u6_s3.ps -E300 -A -P

#plot sensor 21
gmt psbasemap -R0/250/23.2/23.7 -JX10c/6c -V -K -BWeSn -Bxa50g50+l"cycle" -Bya0.1g0.05+l"units">FD003_u6_s21.ps
gmt psxy FD003_u6.txt -R0/250/23.2/23.7 -JX10c/6c -i2,26 -W1,blue -V -O >>FD003_u6_s21.ps
gmt ps2raster -Tg -V FD003_u6_s21.ps -E300 -A -P

