#TEST file

#replace ; with tab
awk  '{gsub(";","\t",$0); print;}' <input/test.csv >temp/test.txt

# cut FD003, remove first col
awk '{if (NR>1 && $1=="FD003") print $0}' <temp/test.txt >temp/temp.txt
awk '{$1=""; print $0}' <temp/temp.txt >temp/FD003.txt

# cut FD003_u1
awk '{if ($1=="1") print $0}' <temp/FD003.txt >temp/FD003_u1.txt
# cut FD003_u2
awk '{if ($1=="2") print $0}' <temp/FD003.txt >temp/FD003_u2.txt
# cut FD003_u3
awk '{if ($1=="3") print $0}' <temp/FD003.txt >temp/FD003_u3.txt
# cut FD003_u4
awk '{if ($1=="4") print $0}' <temp/FD003.txt >temp/FD003_u4.txt
# cut FD003_u5
awk '{if ($1=="5") print $0}' <temp/FD003.txt >temp/FD003_u5.txt

#RUL file

#replace ; with tab
awk  '{gsub(",","\t",$0); print;}' <input/RUL.csv >temp/rul.txt

# cut FD003, remove first col
awk '{if (NR>1 && $1=="FD003") print $0}' <temp/rul.txt >temp/temp.txt
awk '{$1=""; print $0}' <temp/temp.txt >temp/rul_FD003.txt

# cut FD003_u1
awk '{if ($1=="1") print $2}' <temp/rul_FD003.txt >temp/rul_FD003_u1.txt
# cut FD003_u2
awk '{if ($1=="2") print $2}' <temp/rul_FD003.txt >temp/rul_FD003_u2.txt
# cut FD003_u3
awk '{if ($1=="3") print $2}' <temp/rul_FD003.txt >temp/rul_FD003_u3.txt
# cut FD003_u4
awk '{if ($1=="4") print $2}' <temp/rul_FD003.txt >temp/rul_FD003_u4.txt
# cut FD003_u5
awk '{if ($1=="5") print $2}' <temp/rul_FD003.txt >temp/rul_FD003_u5.txt