close all
clear all
%%
%change input file to analyse other units! should be automated of course...
%data
fileID = fopen('temp/FD003_u1.txt','r');
formatSpec = '%f';
numsens = 21;
fmt = ['%d','%d',repmat('%f ',1, numsens+3), '\n'];
sizeA = [26 Inf];
A = fscanf(fileID,fmt,sizeA);
FD003_u1=A';
fclose(fileID);

%rul
fileID = fopen('temp/rul_FD003_u1.txt','r');
formatSpec = '%f';
sizeA = [1 Inf];
A = fscanf(fileID,formatSpec,sizeA);
rul_FD003_u1=A';
fclose(fileID);
%%
%plot unit 1, settings 1-3
for ii = 3:5
    figure;
    plot(FD003_u1(:,ii))
    title('Settings')
end
%plot unit 1, sensors 1, 2, 21 (others looked similar anyway)
for ii = 6
    figure;
    plot(FD003_u1(:,ii))
    title('Sensor 1')
end
for ii = 7
    figure;
    plot(FD003_u1(:,ii))
    title('Sensor 2')
end
for ii = 26
    figure;
    plot(FD003_u1(:,ii))
    title('Sensor 21')
end
%%
%Get unit 1, setting 1 and 2 vectors
FD003_u1_t1(:,1)=FD003_u1(:,3);
FD003_u1_t2(:,1)=FD003_u1(:,4);
%Get unit 1, sensor 2 and 21 vectors
FD003_u1_s2(:,1)=FD003_u1(:,7);
FD003_u1_s21(:,1)=FD003_u1(:,26);
%%
%Correlation between setting 1 and sensor 2
R12=corrcoef(FD003_u1_t1,FD003_u1_s2);
%Correlation between setting 2 and sensor 2
R22=corrcoef(FD003_u1_t2,FD003_u1_s2);
%Correlation between setting 1 and sensor 21
R121=corrcoef(FD003_u1_t1,FD003_u1_s21);
%Correlation between setting 2 and sensor 21
R221=corrcoef(FD003_u1_t2,FD003_u1_s21);
%Conclusion: Settings not very relevant
%%
%Moving average (for smoothing)
window=20;
M_u1_s2=movmean(FD003_u1_s2,window);
figure;
plot(M_u1_s2);
title('Moving average, sensor 2');
M_u1_s21=movmean(FD003_u1_s21,window);
figure;
plot(M_u1_s21);
title('Moving average, sensor 21');
%%
cycles_u1=size(FD003_u1,1)
%criteria
stdev_u1_s2=std(M_u1_s2)
u1_s2_range=M_u1_s2(cycles_u1,1)-M_u1_s2(1,1)
u1_s2_1sigma=M_u1_s2(1,1)+1*stdev_u1_s2
u1_s2_2sigma=M_u1_s2(1,1)+2*stdev_u1_s2
u1_s2_3sigma=M_u1_s2(1,1)+3*stdev_u1_s2
figure
plot(M_u1_s2)
hline=refline([0 u1_s2_1sigma]);
hline=refline([0 u1_s2_2sigma]);
hline=refline([0 u1_s2_3sigma]);

%criteria
stdev_u1_s21=std(M_u1_s21)
u1_s21_range=M_u1_s21(cycles_u1,1)-M_u1_s21(1,1)
u1_s21_1sigma=M_u1_s21(1,1)+1*stdev_u1_s21
u1_s21_2sigma=M_u1_s21(1,1)+2*stdev_u1_s21
u1_s21_3sigma=M_u1_s21(1,1)+3*stdev_u1_s21
figure
plot(M_u1_s21)
hline=refline([0 u1_s21_1sigma]);
hline=refline([0 u1_s21_2sigma]);
hline=refline([0 u1_s21_3sigma]);

%find when the moving average line (first) crosses the 3 sigma line
u1_s2_lim=0;
for i=1:cycles_u1
if M_u1_s2(i,1)<u1_s2_3sigma
   u1_s2_lim=u1_s2_lim+1;
end
end
%cycles after crossing 3sigma
u1_s2_after3s=cycles_u1-u1_s2_lim+rul_FD003_u1;

u1_s21_lim=0;
for i=1:cycles_u1
if M_u1_s21(i,1)<u1_s21_3sigma
   u1_s21_lim=u1_s21_lim+1;
end
end
%cycles after crossing 3sigma
u1_s21_after3s=cycles_u1-u1_s21_lim+rul_FD003_u1